# docs.python.org/fr

<!-- .slide: data-background="static/background.jpg" -->

<br/>

<b>Julien Palard</b>


# Les traductions

- https://afpy.org/doc/python/3.6
- http://docs.python.jp/3/
- http://docs.python.org.ar/tutorial/3/real-index.html
- http://transifex.com/python-doc/
- http://python-lab.ru/documentation/index.html


## Historique du projet

- 2012 : Naissance du projet
- 2013 : ø
- 2014 : ø
- 2015 : Projet relancé (vers décembre)
- 2016 : +20% traduit !!
- 2016 : Idée de docs.python.org/fr/ (bpo-26546)
- 2016 : Thread "docs.python.org/fr/?" sur python-ideas
- 2017 : PEP 545


## Pourquoi

- Je m'ennuie dans le train, alors je traduis…
- L'anglais ne devrait pas être un frein pour apprendre le Python.
- Python pourrait au contraire introduire à l'anglais.


## Github

https://github.com/AFPy/python_doc_fr/
![](static/2016-python_doc_fr_github.png)


## Progression

- 2016-01: 6%
- 2016-02: 11%
- 2016-03: 14%
- 2016-04: 22%
- 2016-05: 23%
- 2016-06: Tentative de merge avec docs.python.org

12 contributeurs depuis 2012


## Github

Progression
![](static/2016-progression_100.png)

## Github
Progression
![](static/2016-progression.png)

## Comment on fait
```bash
$ git clone https://github.com/AFPy/python_doc_fr.git
```
…
```bash
$ poedit 3.6/library.po
```
…
```bash
$ make
```

## Comment on fait

![](static/2016-pyfr.png)

## docs.python.org/fr/

En progrès :

- https://bugs.python.org/issue26546
- https://www.python.org/dev/peps/pep-0545/

## Et après ?

- https://docs.python.org/jp/
- https://docs.python.org/es/
- https://docs.python.org/zh-cn/
- …

## Et après ?

Travailler sur quelques améliorations de sphinx-doc comme:

- Annoncer clairement les paragraphes non-traduits: https://github.com/sphinx-doc/sphinx/issues/1246).
- Afficher un avertissement devant les paragraphes pas à jour (fuzzy).

## Mais, maintenant ?

- https://www.python.org/dev/peps/pep-0545/


# Questions ?
