# Retours sur la PyCon U.S. 2019

<!-- .slide: data-background="static/background.jpg" -->

<br/>

<b>Julien Palard</b>

<tt>Paris.py #19</tt>

## Julien Palard

- Python core dev and documentation expert
- Python teacher and coach at
  - Sup'Internet
  - CRI-Paris
  - Makina Corpus
  - …

# PyCon US

![](static/guido-painting.jpg)

## PyCon US

- Language summit, Education summit
- 2 jours de tutoriels
- 3 jours de confs (6 tracks)
- Open spaces
- 3 jours de sprints
- Évènements : PyLadies Auction, dinners, ...
- Des gens !

Notes:

On ignore ce qu'on ignore, les conférences c'est le bon endroit pour le découvrir.

Et de la bière :]


# Language Summit

![](static/capi.jpg)

Notes:

Audience variée : Des anciens core devs, des nouveaux, des
contributeurs de twisted, Django, BeeWare, on y a parlé de :


## Language Summit

 - L'API C,
 - la communication,
 - le mentoring,
 - les subinterpreters (avec des GIL indépendants),
 - mypyc,
 - inclure les time zones dans la stdlib,
 - [bouger les issues sur github](https://speakerdeck.com/mariatta/pep-581).
 - Circuit Python


Notes:

- API C: Victor
- La com: Comment tenter de faire mieux que pour le Walrus Operator
- Le mentoring: Pablo Galindo Salgado, Emily Morehouse, Lisa Roach, Stéphane Wirtel
  Appel de Victor sur la ML mentoring pour plus de diversité.
- mypyc: Essayez le, c'est pas tout a fait comme cython
- TZ: Problem: Time zone changes are far more frequent than Python releases.
- Circuit Python est un fork de MicroPython (copy code.py et pouf), API hardware différences

# Education Summit

Dr. Charles R. Severance nous a parlé de Coursera et leur tendance à
rendre les cours payants, il se bat donc en sortant ses cours et en
les hébergant lui même :

 - py4e.com
 - dj4e.com

## The Zen of Python
### Open Education Resources

```text
- Content matters but not at the expense of logistics.
- You should never assume that your user looks like you
  or thinks like you. Refuse the temptation to design for your
  youger self.
- Access counts.
- Accept that you are a UX designer.
- Testing in an education environment is better than having a
  kid related to you test it.
- If there are too many Python in-jokes, that's a bad idea,
  they exclude new users.
- If you think soemthing is obvious, explain it anyway,
  it's a good idea — it wasn't obvious for beginners.
- Your contributors always need guidance.
- You will fail, and it will still be worth it.
- Resources that exist are better than resources that don't.
- Resources that can be found are better than resources that can't.
- Resource that can be found and used by your target user are even better. Let's do more of that!
```
By Meg Ray

Notes:

Fun fact: deux speakers ont présentés le même "Zen", vu que Meg Ray était présente (keynotter!)

## Lightning Talks

Les certifications Python, ça en est où ?

Notes:
Le sujet prédominant était le manque de
certification Python, a la louche il en existe deux, elles sont
chères, pas forcément bien faites. On pourrait se bouger pour en faire
une meilleure, reconnue par la communauté. Enfin pas moi.


# La pause

![](static/pycon-group.jpg)


# Pleinières

## Russell Kenth Magee

Les « Python Black Swans ».

Notes:
réfléchissons a un évènement qui pourrait modifier
drastiquement Python, qui nous paraitrait évident s'il survenait. Mais
essayons de le prévoir plutôt que le subir. Surtout s'il est dangereux.

## Russell Kenth Magee

Python dans les navigateurs ?

Notes:

Si oui selon Russell ça se
fera via webassembly, c'est donc a nous de le faire. Il nous a fait
une démo avec beeware d'un programme Python qui tourne sur Android,
iOS, Linux, et dans le navigateur, joli !

## RIP

![](static/RIP-python2.jpg)


# Confs

## Luciano Ramalho

```python
def match(words, search):
    words = words.split()
    search = search.split()
    for to_find in search:
        for word in words:
            if to_find == word:
                break
        else:
            return False
    return True
```

Note:
Luciano est pour moi une grande source d'inspiration autour de l'enseignement de Python.

La conf sur les ensembles, trop peu utilisés ? Ignorés ?


## Luciano Ramalho

```python
def match(words, search):
    return set(words.split()) >= set(search.split())
```
from the doc:

> Test whether every element in other is in the set.


## De Morgan's Law

```python
>>> e = {n for n in range(10) if n % 2 == 0}
>>> p = {*range(5)}
>>> f = {1, 1, 2, 3, 5, 8}
>>> f - (p & e) == (f - p) | (f - e)
True
```

Notes: Luciano nous a montré ça aussi.


## De Morgan's Law

Still not a proof, let's bring pytest and hypothesis!

```python
@given(e=st.sets(st.integers()),
       p=st.sets(st.integers()),
       f=st.sets(st.integers()))
def test_de_morgan(e, p, f):
    assert f - (p & e) == (f - p) | (f - e)
```

Notes: On a eu beaucoup de Hypothesis partout, alors testons avec.


## Lightning Talks

J'ai appris l'existence de pytype, copain de mypy, mais plus orienté inférence.


## Pablo Galindo
### Le ramasse-miettes

Et bah, c'est pas drôle un ramasse miette.


```python
>>> x = None
>>> class Lazarus:
...     def __del__(self):
...         print("I'm going to be resurected!")
...         global x
...         x = self
...
>>> a = Lazarus()
>>> del a
I'm going to be resurected!
>>> x
<__main__.Lazarus object at 0x7fd0a02c0978>
```

# Les sprints

Sujets variés :
 - cpython
 - mailman
 - hypothesis
 - packaging
 - pellets (flask, jinja2, click, ...)
 - mypy / mypyc
 - Circuit Python
 - ...

## Infra avec Ernest W. Durbin III

2FA sur pypi.org \o/

Notes:

Ce n'est pas public pour le moment, demandez moi ou demandez sur
#python-infra sur freenode si vous voulez le tester.


## Les sprints

On a démarré trois nouvelles traductions de la documentation de Python :

- Italien, avec Alessandro Cucci
- Espagnol avec Raul Cumplido
- Hindi (hi_IN) avec Sanyam Khurana

Notes:

- Ca nous mène à 17 traductions plus ou moins démarrées.
- 9 avec une activité non négligable
- 5 en production

## cpython

J'ai principalement bossé sur cpython (segfaults / memory leaks)


# Ce que j'ai retenu ?

- Black, black everywhere!
- F-string debug mode : `foo=5; print(f"{foo=}")`
- Rencontrer les gens !

Notes:

Pour ceux sous Windows, VSCode permet maintenant d'utiliser un venv de
WSL, cherchez "Remote Development" (need VSCode Insiders).

## PSF Q2 2019 Fundraiser

<!-- .slide: data-background="PSF_fundraiser_icon_2019.png" -->

Notes:

Ça finance aussi les ateliers de traduction de la doc :)

Pendant qu'on en parle, vous cotisez à l'AFPy ?

# Et après ?

Prochaine PyCon US à Pittsburgh :

- 15 au 23 avril 2020
- 12 au 20 mai 2021


## Et après ?

En attendant, suivez @pyconfr pour avoir des nouvelles de la prochaine PyCon Fr.

Secret : Si on la fait ce sera peut être à Bordeaux, peut être du 30
octobre au 3 novembre, peut être.
