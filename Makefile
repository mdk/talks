SRCS := $(sort $(wildcard *.md))
DEST := $(addprefix output/, $(SRCS:.md=.html))

.PHONY: static
static: $(DEST)
	if [ -d static ]; then cp -a static/ output/; fi

output/%.html: %.md
	mkdir -p output
	mdtoreveal $< --output $@

.PHONY: rsync
rsync: static
	rsync -vah --delete output/ mdk@mdk.fr:/var/www/mdk.fr/talks/

.PHONY: clean
clean:
	rm -fr output

.PHONY: entr
entr:
	ls -1 *.md | entr -nr $(MAKE) serve

.PHONY: serve
serve: static
	python3 -m http.server -d output/

.PHONY: test
test:
	if [ -f test.py ]; then \
	    python test.py *.md; \
	fi

README.md:
	@printf "# My talks\n\n" > README.md
	@printf -- "- https://mdk.fr/talks/%s\n" $(SRCS:.md=.html) >> README.md
