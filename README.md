# My talks

- https://mdk.fr/talks/2016-pyconfr-i18n.html
- https://mdk.fr/talks/2017-paris.py-13-i18n.html
- https://mdk.fr/talks/2018-paris.py-14-asyncio.html
- https://mdk.fr/talks/2018-paris.py-16-hackinscience.html
- https://mdk.fr/talks/2018-pyconfr-emergence.html
- https://mdk.fr/talks/2019-paris.py-19-retours-sur-la-pycon.html
- https://mdk.fr/talks/2019-write-the-docs-paris-python-translation.html
- https://mdk.fr/talks/2020-en-attendant-la-pycon-fr-perf.html
- https://mdk.fr/talks/2021-en-attendant-la-pycon-fr-packaging.html
- https://mdk.fr/talks/2022-campus-du-libre-hackinscience.html
- https://mdk.fr/talks/2023-pyconfr-sphinx-lint.html
