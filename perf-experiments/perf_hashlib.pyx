# cython: language_level = 3

from libc.stdlib cimport malloc
from libc.string cimport memcpy, strlen, memset

import sys
import hashlib

cpdef search():
    cdef int repeat = 10
    cdef char *suffix = b"AFPy"
    cdef unsigned int size = repeat + strlen(suffix)
    cdef char *candidate = <char *> malloc(size + 1)
    cdef char *digest;
    memset(candidate, b'A', size)
    memcpy(candidate + size - strlen(suffix), suffix, strlen(suffix))
    candidate[size] = 0

    cdef unsigned int i = 0;
    while True:
        candidate[9] = i % 58 + 65;
        candidate[8] = <char>(i / 58 ** 1) % 58 + 65;
        candidate[7] = <char>(i / 58 ** 2) % 58 + 65;
        candidate[6] = <char>(i / 58 ** 3) % 58 + 65;
        candidate[5] = <char>(i / 58 ** 4) % 58 + 65;
        candidate[4] = <char>(i / 58 ** 5) % 58 + 65;
        i += 1
        output_buffer = hashlib.sha512(candidate).hexdigest()
        if output_buffer.startswith("00000"):
            print(f"sha512({candidate!r}) = {output_buffer}")
            sys.exit(0)
    print("Not found")
