#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "openssl/sha.h"

static inline void tohex(char *outputbuffer, unsigned char c)
{
    outputbuffer[1] = "0123456789abcdef"[c % 16];
    outputbuffer[0] = "0123456789abcdef"[c / 16];
}

void sha512(char *string, char outputBuffer[129])
{
    unsigned char hash[SHA512_DIGEST_LENGTH];
    SHA512_CTX sha512;
    SHA512_Init(&sha512);
    SHA512_Update(&sha512, string, strlen(string));
    SHA512_Final(hash, &sha512);
    int i = 0;
    for (i = 0; i < SHA512_DIGEST_LENGTH; i++)
        tohex(outputBuffer + 2 * i, hash[i]);
    outputBuffer[128] = 0;
}

int main(int ac, char **av)
{
    unsigned char outputbuffer[129];
    unsigned char inputbuffer[7];

    int repeat = 10;
    char *suffix = "AFPy";
    int size = repeat + strlen(suffix);
    char *candidate = malloc(size + 1);
    memset(candidate, 'A', size);
    memcpy(candidate + size - strlen(suffix), suffix, strlen(suffix));
    candidate[size] = 0;

    long long int i = 0;
    while (1)
    {
        candidate[9] = i % 58 + 65;
        candidate[8] = (i / 58) % 58 + 65;
        candidate[7] = (i / (58 * 58)) % 58 + 65;
        candidate[6] = (i / (58 * 58 * 58)) % 58 + 65;
        candidate[5] = (i / (58 * 58 * 58 * 58)) % 58 + 65;
        candidate[4] = (i / (58 * 58 * 58 * 58 * 58)) % 58 + 65;
        i += 1;
        sha512(candidate, outputbuffer);
        if (outputbuffer[0] == '0' && outputbuffer[1] == '0' && outputbuffer[2] == '0' && outputbuffer[3] == '0' && outputbuffer[4] == '0') {
            printf("%s\n", outputbuffer);
            return 0;
        }
    }
    return 1;
}
