# Expériences

Toutes les expériences cherchent de la même manière et trouvent le
même résultat (sans quoi les temps ne sont pas comparables: changer un
peu l'alphabet peut permettre de trouver rapidement une bonne
solution, ou de passer à côté d'une solution) :

C'est une recherche d'un sha512 commençant par '00000' d'une chaîne
commençant par un préfix libre de 10 caractères suivi de la chaîne `AFPy`.

Le préfixe utilise tous les caractères ASCII de `a` à `Z`, dans l'ordre ASCII.

Toutes les expériences doivent donc trouver le même résultat :

```
sha512("AAAAAAEfNeAFPy") = 000000c9ddc3e63e34aecc1724fa38d55636a678800250e1bf322c4da065f37f8d251fb68a55bc8ca6ecf1fc226a712a65ae8d8c5d3e11a4527779d74f8fc8b6
```

C'est reproductible en exécutant `run.sh` (avec gcc et cython d'installé).


# Pure C
```
Time (mean ± σ):     298.9 ms ±   7.4 ms    [User: 298.5 ms, System: 0.4 ms]
Range (min … max):   286.8 ms … 313.1 ms    10 runs
```


# Cython sans hashlib

Ressemble beaucoup au C pur.

```
Time (mean ± σ):     819.9 ms ±  24.7 ms    [User: 817.5 ms, System: 2.4 ms]
Range (min … max):   791.4 ms … 867.4 ms    10 runs
```


# Cython avec hashlib

Utilise hashlib, et donc des objets Python et des strings Python, la
conversion a un coût.

```
Time (mean ± σ):      1.312 s ±  0.043 s    [User: 1.309 s, System: 0.003 s]
Range (min … max):    1.258 s …  1.382 s    10 runs
```


# Pure Python

```
Time (mean ± σ):      1.885 s ±  0.066 s    [User: 1.882 s, System: 0.003 s]
Range (min … max):    1.808 s …  2.034 s    10 runs
```


# Pypy 7.3.3

```
Time (mean ± σ):      3.834 s ±  0.054 s    [User: 3.772 s, System: 0.045 s]
Range (min … max):    3.717 s …  3.897 s    10 runs
```
