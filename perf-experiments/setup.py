# python setup.py build_ext --inplace

from setuptools import setup, Extension
from Cython.Build import cythonize

ext_modules = [
    Extension("perf", sources=["perf.pyx"], libraries=["crypto"]),
    Extension("perf_hashlib", sources=["perf_hashlib.pyx"]),
]

setup(name="Perf", ext_modules=cythonize(ext_modules))
