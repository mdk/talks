# docs.python.org/fr

<!-- .slide: data-background="static/background.jpg" -->

<br/>

<b>Julien Palard</b>

<tt>WriteTheDocs Paris 2019</tt>


## Julien Palard

- Python core dev and documentation expert
- Python teacher and coach at:
  - Sup'Internet
  - CRI-Paris
  - Makina Corpus
  - …


## History

![](static/french.python.png)


## History

- Python is 28 years old,
- its doc is written in reStructuredText,
- compiled in HTML, PDF, epub, txt using Sphinx,
- more than one million words,
- daily changes.

Notes:
28 years old as of 2019 (first release 1991).

More than "daily" for 3.7 stable: 170 commits on Doc/ over 120 days.


## History

- 2000: Project frpython on sourceforge
- 2001: Translating Python 2.0
- 2007: Python Doc moves from Latex to Sphinx & rst
- 2010: GSoC Project to add i18n to Sphinx
- 2011: 2% translated on pottle.python.org
- 2012: pottle dead, AFPy team migrates to github
- 2015-12: Resurected the project, alone for one year
- 2016-03: docs.python.org/fr/ on python-ideas.
- 2017-03: PEP 545

Notes:

2000: Latex to Latex, scripté en Python 1.5.2 (populaire à l'époque)
2007: Hello Sphinx, created for Python by Georg Brandl, now used by many like Linux Kernel
2012: Few people contributed during a month, and left for two years.
2015: Alone for one year

https://lists.afpy.org/mailman/private/afpy-membres/2012-September/005747.html
http://frpython.sourceforge.net/


## 2000

![](static/sourceforge.png)

Notes: https://web.archive.org/web/20010302160925/http://sourceforge.net/projects/frpython


## 2001

![](static/2.0.png)

Notes: http://quentel.pierre.free.fr/python-trad/intro.html


## 2012

![](static/pottle.png)


## 2019

![](static/docs.python.org.png)


## Progression

![](static/fr_translation_percent.png)

Notes:
- 2016 jan: very short untranslatable strings
- 2016 june: autofill whatsnew.po
- 2017: PEP 545

# How do we work?

Mandatory meet point is:

 `github.com/python/python-docs-*`

But every language can use their own tools as long as they push on the meet point.


## How do we work?

- Some are using Transifex (ja, zh, pt_BR, ko, ...)
- Some are using git (fr, it, es)
- One could use any other tool…

Notes:
The french fries team (fr,it,es) is using git.


## How do we work, in France?

github and pull requests

![](static/prs.png)

----

## How do we work, in France?

It allows us to review and give feedback

![](static/support.png)

----

## But git is hard!

Yes.

Note: But it looks like it's the mandatory way to contribute to most
open source projects. We want to make the translation a way to learn
how to contribute to an open source project. It's like a git sandbox :)

Also it allows offline work that a lot of us do (in the train).


# Tools

How do we cope with:

- Around 500 ``.po`` files,
- more than one million words,
- 45k paragraphs,
- french in reStructuredText in gettext imbrication?

```
#: ../Doc/library/stdtypes.rst:373
msgid "the greatest :class:`~numbers.Integral` <= *x*"
msgstr "le plus grand :class:`~numbers.Integral` <= *x*"
```

## Tools

### pypi.org/p/pospell

``pospell`` is a tool using hunspell to spell check inside a
``.po`` file while ignoring reStructuredText syntax.


## Tools

### pypi.org/p/powrap

``powrap`` is a tool using ``msgcat`` to rewrap all ``.po`` files to a
fixed width of 79 columns. We're enforcing this wrapping via the CI to
reduce the noise in git logs.


## Tools

### pypi.org/p/potodo

``potodo`` helps us listing what still needs to be done in this mess
of 500 files.  It also synchroniszes with github issues to tell if
someone is already working on a file, avoiding conflicts.


## Tools

### pypi.org/p/pomerge

``pomerge`` helps us propagating translations from a branch to another,
from a repo to another, or simply from a file to another.


## Tools

### poautofill

``poautofill`` uses automatic translation to translate a whole file.
This is bad from so many aspects, but it just helps me to avoid
spending time trying to remember some vocabulary when I'm offline.


# Et après ?

Venez nous aider :

- Sur github.com/python/python-docs-fr
- Aux ateliers mensuels (meetup AFPy Paris)
- Au sprint (2 jours) à La PyCon Fr à Bordeaux à partir du 31 oct 2019.
